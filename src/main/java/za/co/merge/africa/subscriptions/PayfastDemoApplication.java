package za.co.merge.africa.subscriptions;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import za.co.merge.africa.subscriptions.web.dto.response.SubscriptionResponse;

@SpringBootApplication
@RequestMapping("/api/v1/")
public class PayfastDemoApplication {

	private static final String AMPERSAND = "&";
	private static final String EQUALS = "=";

	private static final String DATE_FORMAT = "YYYY-MM-DD'T'HH:MM:SS[+HH:MM]";

	@Value("${client.payfast.merchant-id}")
	private String MERCHANT_ID;

	@Value("${client.payfast.merchant-key}")
	private String MERCHANT_KEY;

	@Value("${client.payfast.ping-uri}")
	private String API_PING_URI;
	
	@Value("${client.payfast.fetch-subscription}")
	private String FETCH_ENDPOINT;

	@Value("${client.payfast.passphrase}")
	private String PASSPHRASE;

	@Value("${client.payfast.api-version}")
	private String API_VERSION;
	
	private final DateTimeFormatter DF = DateTimeFormatter.ofPattern(DATE_FORMAT, Locale.ENGLISH);

	public static void main(String[] args) {
		SpringApplication.run(PayfastDemoApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	public String formatTime() {
		return DF.format(LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault()));
	}

	@GetMapping("ping-payfast")
	public ResponseEntity<String> processSubscriptionQuery(final boolean isTesting) throws UnsupportedEncodingException {
		HttpEntity<String> entity = new HttpEntity<>(constructHttpHeaders());
		return restTemplate().exchange(API_PING_URI, HttpMethod.GET, entity, String.class, "false");
	}
	
	@GetMapping("fetch-sub")
	public ResponseEntity<SubscriptionResponse> querySubscription() throws UnsupportedEncodingException, JsonProcessingException {
		HttpEntity<String> entity = new HttpEntity<>(constructHttpHeaders());
		final ResponseEntity<String> clientResponse = restTemplate().exchange(FETCH_ENDPOINT, HttpMethod.GET, entity, String.class);
		final SubscriptionResponse webResponse = new ObjectMapper().readValue(clientResponse.getBody(), SubscriptionResponse.class);
		return new ResponseEntity<>(webResponse, HttpStatus.OK);
	}
	

	private HttpHeaders constructHttpHeaders() {
		final String timestamp = formatTime();
		HttpHeaders headers = new HttpHeaders();
		headers.set("merchant-id", MERCHANT_ID);
		headers.set("passphrase", PASSPHRASE);
		headers.set("timestamp", timestamp);
		headers.set("version", API_VERSION);
		headers.set("signature", constructSignature(headers));
		return headers;
	}
	
    private String constructSignature(final HttpHeaders headers) {
        StringBuilder signatureConcat = new StringBuilder();
        Map<String, String> headerMap = headers.entrySet().stream()
                .collect(Collectors.toMap(Entry::getKey, e -> String.join(",", e.getValue())));
        NavigableMap<String, String> sortedHeaders = new TreeMap<>(headerMap);
        if (!sortedHeaders.isEmpty()) {
            sortedHeaders.entrySet().forEach(val -> {
                try {
                    signatureConcat.append(val.getKey().concat(EQUALS).concat(URLEncoder.encode(val.getValue(), StandardCharsets.UTF_8.name())));
                    if (!val.getKey().equals(sortedHeaders.lastKey())) {
                        signatureConcat.append(AMPERSAND);
                    }
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
            });
        }
        return DigestUtils.md5Hex(signatureConcat.toString()).toLowerCase();

    }
	
    // Easy way if you have less values to pass
	private String constructSignature(final String timestamp) throws UnsupportedEncodingException {
	    final String UTF_8 = StandardCharsets.UTF_8.name();
		return "merchant-id="
				.concat(URLEncoder.encode(MERCHANT_ID, UTF_8)).concat(AMPERSAND)
				.concat("passphrase=").concat(URLEncoder.encode(PASSPHRASE, UTF_8)).concat(AMPERSAND)
				.concat("timestamp=").concat(URLEncoder.encode(timestamp, UTF_8)).concat(AMPERSAND)
				.concat("version=").concat(URLEncoder.encode(API_VERSION, UTF_8));
	}
}
