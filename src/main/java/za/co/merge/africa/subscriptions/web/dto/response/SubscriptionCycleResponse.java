package za.co.merge.africa.subscriptions.web.dto.response;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubscriptionCycleResponse implements Serializable {

	private static final long serialVersionUID = -8186579275499046496L;
	
	private int amount;
    private int cycles;
    @JsonProperty("cycles_complete")
    private int cyclesComplete;
    private int frequency;
    @JsonProperty("run_date")
    private Date runDate;
    private int status;
    @JsonProperty("status_reason")
    private String statusReason;
    @JsonProperty("status_text")
    private String statusText;
    private String token;
    
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getCycles() {
		return cycles;
	}
	public void setCycles(int cycles) {
		this.cycles = cycles;
	}
	public int getCyclesComplete() {
		return cyclesComplete;
	}
	public void setCyclesComplete(int cyclesComplete) {
		this.cyclesComplete = cyclesComplete;
	}
	public int getFrequency() {
		return frequency;
	}
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	public Date getRunDate() {
		return runDate;
	}
	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getStatusReason() {
		return statusReason;
	}
	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}
	public String getStatusText() {
		return statusText;
	}
	public void setStatusText(String statusText) {
		this.statusText = statusText;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
        
}
