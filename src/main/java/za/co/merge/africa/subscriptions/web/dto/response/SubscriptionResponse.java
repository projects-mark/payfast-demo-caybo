package za.co.merge.africa.subscriptions.web.dto.response;

import java.io.Serializable;

public class SubscriptionResponse implements Serializable {

	private static final long serialVersionUID = 2335484587366431093L;
	
	private int code;
    private String status;
    private DataResponse data;
    
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public DataResponse getData() {
		return data;
	}
	public void setData(DataResponse data) {
		this.data = data;
	}
    
    
}
