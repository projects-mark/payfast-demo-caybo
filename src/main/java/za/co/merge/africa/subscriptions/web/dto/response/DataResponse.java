package za.co.merge.africa.subscriptions.web.dto.response;

import java.io.Serializable;

public class DataResponse implements Serializable {

	private static final long serialVersionUID = -5636822085379096758L;
	
    private SubscriptionCycleResponse response;

	public SubscriptionCycleResponse getResponse() {
		return response;
	}

	public void setResponse(SubscriptionCycleResponse response) {
		this.response = response;
	}
    
}
