# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.6.2/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.6.2/maven-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.6.2/reference/htmlsingle/#boot-features-developing-web-applications)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)

### Example Ping for (https://api.payfast.co.za/ping?testing=false): GET: 
{
	"PayFast API"
}

### Example Response for (https://api.payfast.co.za/subscriptions/:{token}/fetch): GET: 
{
  "code": 200,
  "status": "success",
  "data": {
    "response": {
      "amount": 1000,
      "cycles": 0,
      "frequency": 3,
      "status": 1,
      "token": "${secret_token}",
      "cycles_complete": 1,
      "run_date": "2022-02-05T22:00:00.000+00:00",
      "status_reason": "",
      "status_text": "ACTIVE"
    }
  }
}
